from library import WorldEntity
from data import Position, Sprite, Line, Size, Type, Body

class Player(WorldEntity):
    def __init__(self, position=(0,0), size=(0,0), color=(0,0,0)):
        self.sprite = Sprite(size, color, offset=(size[0]/2, size[1]/2))
        self.position = Position(*position)
        self.body = Body()
        self.size = Size(size)
        self.type = Type("player")
        self.collided = False

class Brush(WorldEntity):
    def __init__(self, size=(0,0), color=(0,0,0)):
        self.line = Line(size, color)
        self.size = Size(size)

class Door(WorldEntity):
    def __init__(self, position=(0,0), size=(0,0), color=(0,0,0)):
        self.sprite = Sprite(size, color)
        self.position = Position(*position)
        self.size = Size(size)

class Obstacle(WorldEntity):
    def __init__(self, position=(0,0), size=(0,0), color=(0,0,0)):
        self.sprite = Sprite(size, color)
        self.position = Position(*position)
        self.size = Size(size)
        self.type = Type("obs")



