import pygame, sys
from ebs import World
from pygame.locals import *
from systems import SpriteSystem, MovementSystem, LineSystem, CollisionSystem
from entities import Player, Brush, Door, Obstacle

running = True
BLACK = (0, 0, 0)
BLUE = (0, 0, 255)
RED = (255, 0, 0)
YELLOW = (246, 213, 84)
WINDOW_SIZE = 360, 640
PLAYER_SIZE = 20, 20
BRUSH_SIZE = 10, 10
EXIT_SIZE = 30, 30
left = False

#sdl initializations
world = World()
window = pygame.display.set_mode(WINDOW_SIZE, 0, 32)
window.fill(BLACK)
pygame.display.set_caption("Don't look back")

#entities
player = Player((WINDOW_SIZE[0]/2, WINDOW_SIZE[1]/2), PLAYER_SIZE, BLUE)
brush = Brush(BRUSH_SIZE, YELLOW)
door = Door((50, 200), EXIT_SIZE, RED)
obstacle = Obstacle((100, 100), (100,20), RED)

#init systems
systems = [
    SpriteSystem(),
    LineSystem(door, player),
    MovementSystem(player),
    CollisionSystem(player, brush),
]

#world
for system in systems:
    world.add_system(system)
