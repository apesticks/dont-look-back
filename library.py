from ebs import Entity
import registry, sys

def debug(*msg):
    if registry.DEBUG == True:
        print("[DEBUG]: " + ", ".join([str(item) for item in msg]))

class WorldEntity(Entity):
    """ Assuming a global world, avoid the need to pass in world repeatedly"""
    def __new__(cls, *args, **kwargs):
        return Entity.__new__(cls, registry.world, *args, **kwargs)

def info(type, value, tb):
    traceback.print_exception(type, value, tb)
    if not isinstance(value, KeyboardInterrupt):
        pdb.pm()
sys.excepthook = info
