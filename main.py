import pygame, registry, sys
from pygame.locals import *

def run():
    pygame.init()
    clock = pygame.time.Clock()

    while registry.running:
        registry.mx, registry.my = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type == QUIT:
                registry.running = False
                break

            if event.type == MOUSEBUTTONDOWN:
                registry.brush.line.draw = True
            if event.type == MOUSEBUTTONUP:
                registry.brush.line.draw = False

            if event.type == KEYUP:
                if event.key == K_r:
                    x, y = (registry.WINDOW_SIZE[0]/2, registry.WINDOW_SIZE[1]/2)
                    registry.player.position.x = x
                    registry.player.position.y = y
                    registry.player.body.collided = False

        clock.tick(60)
        registry.window.fill((0,0,0))
        registry.world.process()
        pygame.display.update()

if __name__ == "__main__":
    sys.exit(run())
