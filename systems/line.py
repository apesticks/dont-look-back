import ebs, registry, pygame
from pygame import Surface
from data import Line

class LineSystem(ebs.Applicator):
    def __init__(self, door, player):
        super(LineSystem, self).__init__()
        self.componenttypes = (Line,)
        self.door = door
        self.player = player

    def can_draw(self, line):
    # print (line.draw,line.traversed,line.connected,self.player.position.at_starting_position())
        return line.draw and not line.traversed and not line.connected and self.player.position.at_starting_position()

    def draw_line(self, line):
        for point in line.points:
            new_surface = Surface(line.size)
            new_surface.fill(line.color)
            registry.window.blit(new_surface, (point[0] - line.size[0]/2, point[1] - line.size[1]/2))

    def new_line_drawn(self, line):
        return line.draw == False and line.length() > 0

    def door_connected(self, line):
        if line.points[-1][0] not in range(self.door.position.x, self.door.position.x + self.door.size[0]):
            return False
        if line.points[-1][1] not in range(self.door.position.y, self.door.position.y + self.door.size[1]):
            return False
        return True

    def player_connected(self, line):
        if line.points[0][0] not in range(self.player.position.starting_x - self.player.size[0]/2, self.player.position.starting_x + self.player.size[0] - self.player.size[0]/2):
            return False
        if line.points[0][1] not in range(self.player.position.starting_y - self.player.size[0]/2, self.player.position.starting_y + self.player.size[1] - self.player.size[0]/2):
            return False
        return True

    def process(self, world, component_sets):
        for item in component_sets:
            line = item[0]
            if self.can_draw(line):
                line.add_point((registry.mx, registry.my))
                self.draw_line(line)
            if self.new_line_drawn(line):
                print (self.door_connected(line), self.player_connected(line))
                if self.door_connected(line) and self.player_connected(line):
                    line.connection_on()
                    line.traversed_off()
                else:
                    line.set_default_state()
