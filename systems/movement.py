import ebs, registry
from data import Line

class MovementSystem(ebs.Applicator):
    def __init__(self, player):
        super(MovementSystem, self).__init__()
        self.componenttypes = (Line,)
        self.body = player.body
        self.position = player.position

    def line_exists(self, line):
        return line.length() > 0 and line.connected

    def line_traversed(self, line):
        return line.index == line.length() and line.length() > 0

    def collision_detected(self):
        return self.body.collided

    def move_on_line(self, line, position):
        points = line.points
        if line.length() > 0 and line.index < line.length():
            position.x = points[line.index][0]
            position.y = points[line.index][1]
            line.index += 1

    def process(self, world, component_sets):
        for item in component_sets:
            line = item[0]
            if self.line_exists(line) and not line.traversed:
                self.move_on_line(line, self.position)
            if self.line_traversed(line) or self.collision_detected():
                line.set_default_state()
