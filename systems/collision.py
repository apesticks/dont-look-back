import ebs, registry
from data import Position, Type, Size

class CollisionSystem(ebs.Applicator):
    def __init__(self, player, brush):
        super(CollisionSystem, self).__init__()
        self.componenttypes = (Position, Type, Size)
        self.player = player
        self.line = brush.line

    def process(self, world, component_sets):
        for position, type, size in component_sets:
            if is_obstacle(type):
                if is_colliding(self.player, position, size):
                    remove_line(self.player, self.line)

def is_obstacle(type):
    return type == "obs"

def is_colliding(player, position, size):
    p_left, p_right = player.position.x, player.position.x + player.size[0]
    p_top, p_bottom = player.position.y, player.position.y + player.size[1]
    obs_left, obs_right = position.x, position.x + size[0]
    obs_top, obs_bottom = position.y, position.y + size[1]

    return p_left < obs_right and p_right > obs_left and p_top < obs_bottom and p_bottom > obs_top

def line_exists(line):
    return line.length() > 0

def remove_line(player, line):
    if line_exists(line):
        player.body.collided = True
