import ebs, registry, pygame
from data import Sprite, Position

class SpriteSystem(ebs.Applicator):
    def __init__(self):
        super(SpriteSystem, self).__init__()
        self.componenttypes = (Sprite, Position)

    def process(self, world, component_sets):
        for sprite, position in component_sets:
            registry.window.blit(sprite.surface, (position.x - sprite.offset[0], position.y - sprite.offset[1]))


