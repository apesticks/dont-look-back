from pygame import Surface

class Type(str): pass
class Name(str): pass
class Size(tuple): pass

class Position(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.starting_x = x
        self.starting_y = y

    def at_starting_position(self):
        return self.x == self.starting_x and self.y == self.starting_y

class Sprite(object):
    def __init__(self, size=(0,0), color=(0,0,0), offset=(0,0)):
        self.surface = Surface(size)
        self.surface.fill(color)
        self.color = color
        self.offset = offset

class Body(object):
    def __init__(self):
        self.collided = False

class Line(object):
    def __init__(self, size, color):
        self.draw = False
        self.size = size
        self.color = color
        self.traversed = False
        self.connected = False
        self.points = []
        self.index = 0

    def add_point(self, point):
        if point not in self.points:
            self.points.append(point)

    def delete(self):
        self.points = []

    def length(self):
        return len(self.points)

    def traversed_on(self):
        self.traversed = True

    def traversed_off(self):
        self.traversed = False

    def connection_on(self):
        self.connected = True

    def connection_off(self):
        self.connected = False

    def set_default_state(self):
        self.delete()
        self.traversed_off()
        self.connection_off()
        self.index = 0
